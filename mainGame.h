#ifndef mainGame_h_
#define mainGame_h_

void gameloop ();

extern SDL_Window* g_wind;
extern SDL_Renderer* g_rend;
extern SDL_Rect g_rectangle;

static int fullscreen;
struct fpsType {
	Uint32 msLastFrame;
	Uint32 countedFrames;
};
static struct fpsType fpsValues;

void init();
void gameloop();
void quit();
void clears(SDL_Renderer* rend);

#endif /* mainGame_h_ */
