#include <SDL2/SDL.h>
#include "mainGame.h"

void calc_fps_and_show() {
	// Check and process I/O events
	if (fpsValues.countedFrames == 5) {
		fpsValues.countedFrames = 0;
		Uint32 msps = SDL_GetTicks() - fpsValues.msLastFrame;
		float fps = 1/(float)msps*1000*5; // averaged over 5 fps
		SDL_Log("%.2f fps", fps);
		fpsValues.msLastFrame = SDL_GetTicks();
	}
	
	fpsValues.countedFrames++;
}


void gameloop() {
	SDL_Event event;
	int running = 1;
	SDL_FlushEvent(SDL_ENABLE);
	
	while (running) {
    //ClearScreen(g_main_renderer);
		
		SDL_Delay(1);
		
		calc_fps_and_show();
    if (SDL_PollEvent(&event)) {
      switch (event.type) {

				
			  case SDL_KEYDOWN: {
					running = (event.key.keysym.scancode != SDL_SCANCODE_ESCAPE);
					clears(g_rend);
					SDL_SetRenderDrawColor(g_rend, 23, 0, 255, 255);
					switch (event.key.keysym.scancode) {
						//-- wasd
				    case SDL_SCANCODE_D:
							g_rectangle.x++;
							break;

						case SDL_SCANCODE_W:
							g_rectangle.y--;
							break;
							
					  case SDL_SCANCODE_A:
						  g_rectangle.x--;
							break;
							
					  case SDL_SCANCODE_S:
							g_rectangle.y++;
							break;

						//-- Fullscreen handling
					  case SDL_SCANCODE_F:
							fullscreen = !fullscreen;
							SDL_SetWindowFullscreen(g_wind, fullscreen);
					}

					
					SDL_RenderFillRect(g_rend, &g_rectangle);
				} // close case sdl keydown

			  case SDL_MOUSEMOTION: {
					clears(g_rend);
					SDL_SetRenderDrawColor(g_rend, 23, 0, 255, 255);
					g_rectangle.x = event.motion.x;
					g_rectangle.y = event.motion.y;
					SDL_RenderFillRect(g_rend, &g_rectangle);
					SDL_FlushEvent(SDL_MOUSEMOTION);
				}
				SDL_RenderPresent(g_rend);
			}
		}
	}
}

