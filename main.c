#include <stdio.h>
#include <SDL2/SDL.h>
#include "mainGame.h"

SDL_Window* g_wind;
SDL_Renderer* g_rend;

const SDL_Color GREEN = { 0, 255, 0, SDL_ALPHA_OPAQUE };
const SDL_Color BLACK = { 0, 0, 0, SDL_ALPHA_OPAQUE };
SDL_Rect g_rectangle = {.x=0, .y=0, .w=20, .h=10};

int main(int argc, char* argv[]) {

	init();
	gameloop();
	quit();
	
	return 0;
}

void init () {
	
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
    SDL_Log("SDL_Init(): %s", SDL_GetError());
		return;
  }

	g_wind = SDL_CreateWindow(
    "Losstt",
    SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED,
    800,
    600,
    SDL_WINDOW_OPENGL
  );

	g_rend = SDL_CreateRenderer(g_wind, -1, SDL_RENDERER_PRESENTVSYNC);

	if (g_rend == NULL)
		SDL_Log("failed to create renderer: %s", SDL_GetError());

	SDL_ShowCursor(SDL_DISABLE);
	
	clears(g_rend);
}

void quit()
{
	SDL_DestroyWindow(g_wind);
	SDL_DestroyRenderer(g_rend);
  SDL_Quit();
}

void clears(SDL_Renderer* rend)
{
  SDL_SetRenderDrawColor(g_rend, 0, 0, 0, 255);
	SDL_RenderClear(g_rend);
}
